package betterwithengineering.ie;

import betterwithmods.common.BWMBlocks;
import betterwithmods.common.BWRegistry;
import betterwithmods.common.blocks.mini.BlockCorner;
import betterwithmods.common.blocks.mini.BlockMoulding;
import betterwithmods.common.blocks.mini.BlockSiding;
import betterwithmods.common.blocks.mini.ItemBlockMini;
import betterwithmods.common.items.ItemMaterial;
import betterwithmods.module.Feature;
import betterwithmods.module.gameplay.miniblocks.MiniBlockIngredient;
import betterwithmods.module.gameplay.miniblocks.MiniBlocks;
import blusunrize.immersiveengineering.ImmersiveEngineering;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreIngredient;


public class TreatedWood extends Feature {

    @Deprecated
    public static Block SIDING = new BlockSiding(Material.WOOD) {
        @Override
        public int getUsedTypes() {
            return 3;
        }

    }.setRegistryName("ie_siding");
    @Deprecated
    public static Block MOULDING = new BlockMoulding(Material.WOOD) {
        @Override
        public int getUsedTypes() {
            return 3;
        }

    }.setRegistryName("ie_moulding");
    @Deprecated
    public static Block CORNER = new BlockCorner(Material.WOOD) {
        @Override
        public int getUsedTypes() {
            return 3;
        }
    }.setRegistryName("ie_corner");
    protected final String modid;

    public TreatedWood() {
        configName = "TreatedWood";
        modid = "immersiveengineering";
    }

    @Override
    public void preInit(FMLPreInitializationEvent event) {
        BWMBlocks.registerBlock(SIDING, new ItemBlockMini(SIDING));
        BWMBlocks.registerBlock(MOULDING, new ItemBlockMini(MOULDING));
        BWMBlocks.registerBlock(CORNER, new ItemBlockMini(CORNER));
        MiniBlocks.addWhitelistedBlock(new ResourceLocation(ImmersiveEngineering.MODID, "treated_wood"));
    }

    @Deprecated
    @SideOnly(Side.CLIENT)
    @Override
    public void preInitClient(FMLPreInitializationEvent event) {

        BWMBlocks.setInventoryModel(SIDING);
        BWMBlocks.setInventoryModel(MOULDING);
        BWMBlocks.setInventoryModel(CORNER);
    }


}
